## Group
1. Rim Rakhimov
2. Nikolai Mikriukov
3. Vyacheslav Goreev
4. Daria Vaskovskaya

## Usage
### Setting
The private key should be specified in the _private_key.json_ file. 
Currently the private key used for demo is specified.

Any key that corresponds to address with sufficient balance can be used.  

### Commands
1. To deploy a new contract a POST request should be sent 
to the _deploy_ server with parameters _name_, _symbol_, and _total_supply_ specified: 

    `curl -d 'name=Dlt_Lab_12&symbol=DLT&total_supply=100000' http://127.0.0.1:5000/deploy`
 
1. To set already existent contract as a contract to be used send a 
POST request to the _set_contract_ server with parameter _address_ specified:
    
    `curl -d 'address=0xdFbfb84640b804C9F6947a2c46eCf13Bf1Bc7F63' http://127.0.0.1:5000/set_contract` 
 
1. To transfer tokens to another user send a POST request to the _transfer_ server
with parameters _to_ and _amount_ specified: 

    `curl -d 'to=0xE80290DA1f95E50dD4814F6A76b6149631042905&amount=1234' http://127.0.0.1:5000/transfer`
   
1. To query token metadata open a server in a browser with _query/_ suffix:
    
    `http://127.0.0.1:5000/query`
    
1. To get the balance of a specified uses open a server in a browser with
_get_balance/_ suffix and parameter _address_ specified:

    `http://127.0.0.1:5000/get_balance?address=0xE80290DA1f95E50dD4814F6A76b6149631042905` 

## Demo
https://drive.google.com/file/d/1sDvial59M-9WzxsGfoPuaae-5JiRLSM-/view?usp=sharing