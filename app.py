from os import path
from json import load
from web3 import Web3, HTTPProvider

from flask import Flask, request, make_response

app = Flask(__name__)

w3 = Web3(HTTPProvider("https://ropsten.infura.io/v3/1ebcf809d64347e880264e9c8b00fa55"))


# # TODO: return None
def get_contract_interface():
    contract_interface_path = "./contract.json"
    with open(contract_interface_path) as file:
        return load(file)


contract = None


# get private key from json file
def get_private_key():
    private_key_path = "./private_key.json"
    if path.isfile(private_key_path):
        with open(private_key_path) as file:
            return load(file)["key"]
    else:
        print("Private key has not been found")
        return None


def get_contract_interface():
    contract_interface_path = "./contract.json"
    with open(contract_interface_path) as file:
        return load(file)


def deploy_contract(w3, account, contract_interface, *params):
    tx = w3.eth.contract(
        abi=contract_interface['abi'],
        bytecode=contract_interface['bin']).constructor(*params).buildTransaction({
            "nonce": w3.eth.getTransactionCount(account.address),
            'gasPrice': w3.toWei('10', 'wei')
    })
    signed_tx = account.signTransaction(tx)

    tx_hash = w3.eth.sendRawTransaction(signed_tx.rawTransaction)
    tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)

    contract_address = tx_receipt["contractAddress"]

    return w3.eth.contract(address=contract_address, abi=contract_interface["abi"])


@app.route("/")
def index():
    return '<a href="/query">query</a>'


@app.route("/deploy", methods=['POST'])
def deploy():
    if request.method == 'POST':
        symbol = request.form.get('symbol')
        if symbol is None:
            return "symbol parameter has not been provided\n"
        name = request.form.get('name')
        if name is None:
            return "name parameter has not been provided\n"
        total_supply = request.form.get('total_supply')
        if total_supply is None:
            return "total_supply parameter has not been provided\n"

        private_key = get_private_key()
        if private_key is None:
            return "Private key has not been found"
        account = w3.eth.account.from_key(private_key)

        contract_interface = get_contract_interface()

        global contract
        try:
            contract = deploy_contract(w3, account, contract_interface, name, symbol, int(total_supply))
        except ValueError as e:
            return str(e) + "\n"
        except TypeError as e:
            return str(e) + "\n"
        except:
            return "Internal server error\n"

        return f"Contract has been deployed at address {contract.address}\n"


@app.route("/set_contract", methods=['POST'])
def set_contract():
    contract_address = request.form.get('address')
    if contract_address is None:
        return "Contract address is required"
    if not w3.isAddress(contract_address):
        return "Invalid contract address"

    global contract
    contract = w3.eth.contract(address=contract_address, abi=get_contract_interface()["abi"])

    return f"Contract successfully set to {contract.address}\n"


@app.route("/query", methods=['GET'])
def query():
    if contract is None:
        return "Contract has not been deployed yet"

    try:
        name = contract.functions.name().call()
        symbol = contract.functions.symbol().call()
        total = contract.functions.totalSupply().call()
    except ValueError as e:
        return str(e) + "\n"
    except:
        return "Internal server error\n"

    response = make_response(f"""
Name: {name}
Symbol: {symbol}
Total supply: {total}\n
""")
    response.headers["content-type"] = "text/plain"
    return response


@app.route("/get_balance", methods=['GET'])
def get_balance():
    if contract is None:
        return "Contract has not been deployed yet"

    address = request.args.get('address')
    if address is None:
        return "Provide address to get balance of"
    if not w3.isAddress(address):
        return "Invalid address provided"

    try:
        balance = contract.functions.balanceOf(address).call()
    except ValueError as e:
        return str(e) + "\n"
    except:
        return "Internal server error\n"
    return f"Balance of {address} is {balance}"


@app.route("/transfer", methods=['POST'])
def transfer():
    if contract is None:
        return "Contract has not been deployed yet"

    to = request.form.get('to')
    if to is None:
        return "Provide address which the tokens should be sent to"
    if not w3.isAddress(to):
        return "Invalid address to which tokens should be sent"

    amount = request.form.get('amount')
    if amount is None:
        return "Provide how much tokens should be sent\n"

    private_key = get_private_key()
    if private_key is None:
        return "Private key has not been found\n"
    account = w3.eth.account.from_key(private_key)

    try:
        tx = contract.functions.transfer(to, int(amount)).buildTransaction({
            "nonce": w3.eth.getTransactionCount(account.address),
            'gasPrice': w3.toWei('10', 'wei'),
            'gas': 2000000
        })

        signed_tx = account.signTransaction(tx)

        tx_hash = w3.eth.sendRawTransaction(signed_tx.rawTransaction)
        _ = w3.eth.waitForTransactionReceipt(tx_hash)
        return "Success\n"
    except ValueError as e:
        return "Fail: " + str(e)
    except:
        return "Internal server error\n"
